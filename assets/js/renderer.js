var canvas = document.createElement('canvas');
var ctx = canvas.getContext('2d');
function renderSimple(image) {
	canvas.width = image.width;
	canvas.height = image.height;
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	ctx.drawImage(image, 0, 0);
	return canvas.toDataURL();
}

function render(image, colors) {
	canvas.width = image.width;
	canvas.height = image.height;
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	ctx.drawImage(image, 0, 0);

	var imageData = ctx.getImageData(0,0,image.width, image.height);
	var data = imageData.data;

	for(var i=0; i < data.length; i+=4) {
		var keep = false;
		for(var c = 0; c < 3; c++) {
			if(colors[c] && data[i+c] > 0) {
				keep = true;
				break;
			}
		}
		if(keep)
			data[i] = data[i+1] = data[i+2] = 0;
		else
			data[i+3] = 0;
		
	}

	ctx.putImageData(imageData, 0, 0);
	return canvas.toDataURL();
}