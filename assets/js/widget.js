angular.module('App').directive('widget', function() {
	return {
		templateUrl: 'templates/widget.html',
		scope: {},
		link: function(scope, element, attrs) {
			scope.filters = [{ name: 'Default', colors: [false,true,true] }];
			scope.currentFilter = scope.filters[0];
			
			scope.createFilter = function() {
				scope.filters.push({ name: 'Default ' + scope.filters.length, colors: [false,true,true] });
				scope.changeFilter(scope.filters.length-1);
			}	

			scope.changeFilter = function(index) {
				scope.currentFilter = scope.filters[index];
				filteredImage.src = render(image, scope.currentFilter.colors);
			}

			scope.addColor = function(color) {
				scope.currentFilter.colors[color] = !scope.currentFilter.colors[color];
				filteredImage.src = render(image, scope.currentFilter.colors);
			}

			var filteredImage = q(element, 'img.filtered-image');
			var originalImage = q(element, 'img.original-image');
			var image = new Image();
			
			image.onload = function() {
				originalImage.src = renderSimple(image);
				filteredImage.src = render(image, scope.currentFilter.colors);
			}

			var uploader = q(element, '.file-uploader');
			uploader.addEventListener('change', function(e) {
				var reader = new FileReader();
				reader.onload = function(e) {
					image.src = e.target.result;
				}
				reader.readAsDataURL(e.target.files[0]);
			}, false);
		}
	}
});