var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var concat = require('gulp-concat');


gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: './'
		}
	});
});

gulp.task('sass', function() {
	gulp.src('./assets/sass/**/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('./css'))
		.pipe(reload({ stream: true }));
});

gulp.task('js', function() {
	gulp.src('./assets/js/**/*.js')
		.pipe(concat('app.js'))
		.pipe(gulp.dest('./js'))
		.pipe(reload({ stream: true }));
});

gulp.task('default', ['sass', 'js', 'browser-sync'], function() {
	gulp.watch('./assets/sass/**/*.scss', ['sass']);
	gulp.watch('./assets/js/**/*.js', ['js', browserSync.reload]);
});